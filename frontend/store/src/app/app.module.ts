import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreComponent } from './store/store.component';
import { ProductsComponent } from './store/pages/products/products.component';
import { CustomerComponent } from './store/pages/customer/customer.component';
import { CartComponent } from './store/pages/cart/cart.component';
import { OrderComponent } from './store/pages/order/order.component';

import { HttpClientModule } from '@angular/common/http';
import { BsNavbarComponent } from './bs-navbar/bs-navbar.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { OrderSuccesComponent } from './order-success/order-succes.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { AdminProductsComponent } from './admin/admin-products/admin-products.component';
import { AdminOrdersComponent } from './admin/admin-orders/admin-orders.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductFormComponent } from './admin/product-form/product-form.component';
import { FormsModule } from '@angular/forms';
import { ProductService } from './store/services/product.service';

import { CustomFormsModule } from 'ng2-validation';
import { ProductCardComponent } from './product-card/product-card.component';
import { CartService } from './store/services/cart.service';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { RegistrationService } from './store/services/registration.service';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { TokenStorageService } from './store/services/token-storage.service';
import { AuthGuard } from './_helpers/auth.guard';
import { ShippingFormComponent } from './shipping-form/shipping-form.component';
import { ShippingAddressService } from './store/services/shipping-address.service';
import { AddressListComponent } from './address-list/address-list.component';

@NgModule({
  declarations: [
    AppComponent,
    StoreComponent,
    ProductsComponent,
    CustomerComponent,
    CartComponent,
    OrderComponent,
    BsNavbarComponent,
    LoginComponent,
    HomeComponent,
    ShoppingCartComponent,
    CheckOutComponent,
    OrderSuccesComponent,
    MyOrdersComponent,
    AdminProductsComponent,
    AdminOrdersComponent,
    ProductFormComponent,
    ProductCardComponent,
    RegistrationFormComponent,
    ShippingFormComponent,
    AddressListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    CustomFormsModule
  ],
  providers: [
    authInterceptorProviders,
    AuthGuard,
    TokenStorageService,
    ProductService,
    CartService,
    RegistrationService,
    ShippingAddressService],


  bootstrap: [AppComponent]
})
export class AppModule { }
