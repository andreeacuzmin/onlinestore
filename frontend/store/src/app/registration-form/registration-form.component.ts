import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Customer } from '../store/models/Customer';


import { RegistrationService } from '../store/services/registration.service';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})
export class RegistrationFormComponent implements OnInit {
  customer: Customer = new Customer();
  errorMessage = '';


  constructor(
    private router: Router,
    private registrationService: RegistrationService) {

  }

  ngOnInit(): void {

  }

  save() {
    this.registrationService.addCustomer(this.customer);
    this.router.navigate(['/login']);
    (err: { error: { message: string; }; }) => {
      this.errorMessage = err.error.message;
    }
  }

}
