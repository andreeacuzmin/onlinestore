import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Cart } from '../store/models/Cart';
import { Customer } from '../store/models/Customer';
import { ShippingAddress } from '../store/models/ShippingAddress';
import { CartService } from '../store/services/cart.service';
import { OrderService } from '../store/services/order.service';
import { RegistrationService } from '../store/services/registration.service';
import { ShippingAddressService } from '../store/services/shipping-address.service';
import { TokenStorageService } from '../store/services/token-storage.service';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.scss']
})
export class CheckOutComponent implements OnInit {

  cart$: Observable<Cart>;
  customer$: Observable<Customer>;
  addresses$: any;
  selectedAddressId: number = 0;

  constructor(
    private regService: RegistrationService,
    private cartService: CartService,
    private shippingService: ShippingAddressService,
    private orderService: OrderService,
    private router: Router
  ) {
    this.cart$ = this.cartService.getCart().pipe(take(1)).pipe(map(
      (data) => new Cart(data.cartId, data.customer, data.cartItems, data.total)
    ));

    this.customer$ = this.regService.getCurrentCustomer();

    this.addresses$ = this.shippingService.getAddressList();
  }

  ngOnInit(): void {

  }

  placeOrder() {
    this.orderService.addOrder(this.selectedAddressId).subscribe(
      x => this.router.navigate(['/order-succes']),
      x => alert("There was an error placing the order")
    );
  }

  changeAddress(e: any) {
    this.selectedAddressId = e.target.value;
    console.log("Selected address: " + this.selectedAddressId);
  }
}
