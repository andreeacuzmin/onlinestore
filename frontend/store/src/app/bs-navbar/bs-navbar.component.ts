import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { Cart } from '../store/models/Cart';
import { CartService } from '../store/services/cart.service';
import { RegistrationService } from '../store/services/registration.service';
import { TokenStorageService } from '../store/services/token-storage.service';

@Component({
  selector: 'app-bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.scss']
})
export class BsNavbarComponent implements OnInit {

  cart$: Observable<Cart>;

  //Observable-> este intors de hhtpClient; poti sa te abonezi la el sa vezi daca iti intoarce ceva

  constructor(private registrationService: RegistrationService,
    private tokenStorageService: TokenStorageService,
    private cartService: CartService) {
    this.cart$ = this.cartService.getCart().pipe(take(1)).pipe(map(
      (data) => new Cart(data.cartId, data.customer, data.cartItems, data.total)
    ));
  }

  ngOnInit(): void {

  }

  getFullName(): string {
    let fullName = this.tokenStorageService.getUser()?.fullName;
    return fullName ? fullName : "Anonymous user";
  }

  logout(): void {
    this.registrationService.logout();
  }

  isLoggedIn(): boolean {
    return this.tokenStorageService.getToken() != null;
  }

  isAdmin(): boolean {
    return this.tokenStorageService.getUser().role == "ADMIN";
  }

}
