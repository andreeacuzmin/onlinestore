import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product } from '../models/Product';

const httpOptions = {
  headers: new HttpHeaders({ 'content-type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }


  getProducts() {
    return this.http.get<Product[]>('/server/product');
  }

  getProductById(id: number) {
    return this.http.get<Product>('/server/product/get/' + id);
  }

  addProduct(product: Product) {
    let r = this.http.post<Product>('/server/product/add', product, httpOptions);
    r.subscribe(
      x => console.log("Add success " + x),
      x => console.log("Error: " + x)
    );
  }

  deleteProduct(id: number) {
    this.http.delete('/server/product/delete/' + id).subscribe(
      x => console.log("Delete with success " + x),
      x => console.log("Error: " + x)
    );
  }

  updateProduct(product: Product) {
    this.http.put('/server/product/update', product, httpOptions).subscribe(
      x => console.log("Update with success " + x),
      x => console.log("Error: " + x)
    );
  }



}
