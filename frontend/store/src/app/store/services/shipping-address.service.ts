import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ShippingAddress } from '../models/ShippingAddress';
import { TokenStorageService } from './token-storage.service';

const httpOptions = {
  headers: new HttpHeaders({ 'content-type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ShippingAddressService {

  constructor(private http: HttpClient, private tokenStorageService: TokenStorageService) {
  }

  getAddressList() {
    let userId = this.tokenStorageService.getUser().id;
    return this.getAddressByUserId(userId);
  }

  getAddressByUserId(id: number) {
    return this.http.get<ShippingAddress>('/server/shippingAddress/getByUserId/' + id);
  }

  getAddressId(id: number) {
    return this.http.get<ShippingAddress>('/server/shippingAddress/get/' + id);
  }

  addAddress(shippingAddress: ShippingAddress) {
    let userId = this.tokenStorageService.getUser().id;
    shippingAddress.customer = userId;
    let r = this.http.post<ShippingAddress>('/server/shippingAddress/add', shippingAddress, httpOptions);
    r.subscribe(
      x => console.log("Add success " + x),
      x => console.log("Error: " + x)
    );
  }

  deleteAddress(id: number) {
    this.http.delete('/server/shippingAddress/delete/' + id).subscribe(
      x => console.log("Delete with success " + x),
      x => console.log("Error: " + x)
    );
  }

  updateAddress(shippingAddress: ShippingAddress) {
    this.http.put('/server/shippingAddress/update', shippingAddress, httpOptions).subscribe(
      x => console.log("Update with success " + x),
      x => console.log("Error: " + x)
    );
  }
}
