import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Order } from '../models/Order';
import { TokenStorageService } from './token-storage.service';

const httpOptions = {
  headers: new HttpHeaders({ 'content-type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient, private tokenStorageService: TokenStorageService) { }

  getAllOrders() {
    return this.http.get<Order[]>('/server/order');
  }

  getOrderList() {
    let userId = this.tokenStorageService.getUser().id;
    return this.findOrdersByCustomer(userId);
  }

  findOrdersByCustomer(id: number) {
    return this.http.get<Order[]>('/server/order/getByCustomer/' + id);
  }

  getOrderById(id: number) {
    return this.http.get<Order>('/server/order/get/' + id);
  }

  confirmOrderById(id: number) {
    return this.http.put('/server/order/confirm/' + id, null);
  }

  addOrder(shippingAddressId: number) {
    let userId = this.tokenStorageService.getUser().id;
    return this.http.post<Order>('/server/order/add/' + userId + '/' + shippingAddressId, httpOptions);
  }


}
