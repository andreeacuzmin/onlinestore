import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Customer } from '../models/Customer';
import { Observable } from 'rxjs';
import { TokenStorageService } from './token-storage.service';

const httpOptions = {
  headers: new HttpHeaders({ 'content-type': 'application/json' })
};

const httpOptionsLogin = {
  headers: new HttpHeaders({ 'content-type': 'application/json' }),
  observe: "response" as const
};

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient,
    private tokenStorageService: TokenStorageService) { }

  addCustomer(customer: Customer) {
    let r = this.http.post<Customer>('/server/customers/add', customer, httpOptions);
    r.subscribe(
      x => console.log("Add success " + JSON.stringify(x)),
      x => { console.log("Error: " + JSON.stringify(x)); alert("There has been an error adding the account!") }
    );
  }

  getCurrentCustomer() {
    let userId = this.tokenStorageService.getUser().id;
    return this.getCustomerById(userId);
  }

  getCustomers() {
    return this.http.get<Customer[]>('/server/customers');
  }

  getCustomerById(id: number) {
    return this.http.get<Customer>('/server/customers/get/' + id);
  }

  getCustomerByUsername(userName: string) {
    return this.http.get<Customer>('/server/customers/get/' + userName);
  }

  deleteCustomer(id: number) {
    this.http.delete('/server/customers/delete/' + id).subscribe(
      x => console.log("Delete with success " + JSON.stringify(x)),
      x => console.log("Error: " + JSON.stringify(x))
    );
  }

  login(username: string, password: string): Observable<any> {
    return this.http.post('/server/login',
      {
        'username': username,
        'password': password
      }, httpOptionsLogin);
  }

  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
}
