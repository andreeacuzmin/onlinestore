import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cart } from '../models/Cart';
import { CartItem } from '../models/CartItem';
import { TokenStorageService } from './token-storage.service';

const httpOptions = {
  headers: new HttpHeaders({ 'content-type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private http: HttpClient, private tokenStorageService: TokenStorageService) { }

  getCart() {
    let userId = this.tokenStorageService.getUser().id;
    return this.getCartById(userId);
  }

  // getCartById(id: number): Cart | null {

  getCartById(id: number) {
    return this.http.get<Cart>('/server/customer/cart/get/' + id);
  }

  addCartItem(id: number, cartItem: CartItem) {
    let r = this.http.post<CartItem>('/server/customer/cart/add/' + id, cartItem, httpOptions);
    r.subscribe(
      x => console.log("Add success " + x),
      x => console.log("Error: " + x)
    );
  }

  deleteCartItem(id: number) {
    this.http.delete('/server/customer/cart/delete/' + id).subscribe(
      x => console.log("Delete with success " + x),
      x => console.log("Error: " + x)
    );
  }

  deleteAllCartItems() {
    let userId = this.tokenStorageService.getUser().id;
    this.http.delete('/server/customer/cart/clearCart/' + userId).subscribe(
      x => console.log("Delete with success " + x),
      x => console.log("Error: " + x)
    );
  }

}
