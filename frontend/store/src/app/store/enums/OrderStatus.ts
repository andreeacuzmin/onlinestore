export enum OrderStatus {
    "New Order",
    "Processing",
    "Finished",
    "Canceled"
}