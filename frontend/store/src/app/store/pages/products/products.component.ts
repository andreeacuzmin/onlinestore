import { OnDestroy } from '@angular/core';
import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { Product } from '../../models/Product';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnDestroy {
  // products$;
  products: Product[] = [];
  filteredProducts: Product[] = [];
  subscription: Subscription;

  constructor(productService: ProductService) {
    this.subscription = productService.getProducts()
      .subscribe(products => {
        this.filteredProducts = this.products = products;

      });
  }

  filter(query: string, min: string, max: string) {
    // console.log(query);
    this.filteredProducts = (query) ?
      this.products.filter(p => p.productName.toLowerCase()
        .includes(query.toLowerCase())) :
      this.products;

    this.filteredProducts = (min) ?
      this.filteredProducts.filter(p => p.productPrice >= parseFloat(min)) :
      this.filteredProducts;

    this.filteredProducts = (max) ?
      this.filteredProducts.filter(p => p.productPrice <= parseFloat(max)) :
      this.filteredProducts;
  }




  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
