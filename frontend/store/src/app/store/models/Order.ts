import { Customer } from "./Customer";
import { OrderStatus } from "../enums/OrderStatus";

export class Order {

    constructor(
        public customerOrderId: number = 0,
        public orderStatus: OrderStatus = orderStatus,
        public customer: Customer = customer,
        public datePlaced: number = new Date().getTime()
    ) {

    }
}