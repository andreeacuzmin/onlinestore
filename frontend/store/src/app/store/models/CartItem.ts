
export class ProductInfo {
    constructor(
        public productId: number = 0,
        public productName: string = "",
        public productPrice: number = 0,
        public imageUrl: string = ""
    ) {
    }


}

export class CartItem {

    constructor(

        public quantity: number,
        public productInfo: ProductInfo,
        public cartItemId: number = 0,
        public total: number = 0

    ) {

    }



}