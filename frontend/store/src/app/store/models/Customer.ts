import { ApplicationUserRole } from "../enums/ApplicationUserRole";
import { Cart } from "./Cart";
import { Order } from "./Order";
import { ShippingAddress } from "./ShippingAddress";

export class Customer {

    constructor(
        public customerId: number = 0,
        public customerName: string = "",
        public nickname: string = "",
        public customerEmail: string = "",
        public customerPhone: string = "",
        public password: string = "",
        public shippingAddress: ShippingAddress[] = [],
        public order: Order[] = [],
        public role: ApplicationUserRole = ApplicationUserRole.CUSTOMER
    ) {


    }
}