import { Customer } from "./Customer";
import { CartItem } from "./CartItem";


export class Cart {


    constructor(
        public cartId: number,
        public customer: Customer,
        public cartItems: CartItem[],
        public total: number) {
    }

    get totalItemsCount(): number {
        let count = 0;
        for (let productId in this.cartItems) {
            count += this.cartItems[productId].quantity;
        }
        return count;
    }


}