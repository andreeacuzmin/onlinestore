export class Product {


    constructor(
        public productId: number = 0,
        public productName: string = '',
        public productDescription: string = '',
        public productPrice: number = 0,
        public productStock: number = 0,
        public imageUrl: string = ''
    ) {

    }

}