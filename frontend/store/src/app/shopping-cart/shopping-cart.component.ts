import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Cart } from '../store/models/Cart';
import { CartService } from '../store/services/cart.service';


@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent {
  cart$: Observable<Cart>;


  constructor(private cartService: CartService) {
    this.cart$ = this.cartService.getCart().pipe(take(1)).pipe(map(
      (data) => new Cart(data.cartId, data.customer, data.cartItems, data.total)
    ));
  }

  deleteCartItem(id: number) {
    this.cartService.deleteCartItem(id);
    window.location.reload();
  }

  deleteAllCartItems() {
    this.cartService.deleteAllCartItems();
    window.location.reload();
  }

}
