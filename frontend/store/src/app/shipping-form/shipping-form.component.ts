import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ShippingAddress } from '../store/models/ShippingAddress';
import { ShippingAddressService } from '../store/services/shipping-address.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-shipping-form',
  templateUrl: './shipping-form.component.html',
  styleUrls: ['./shipping-form.component.scss']
})
export class ShippingFormComponent implements OnInit {

  shipping: ShippingAddress = new ShippingAddress();
  id: number = 0;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private shippingAddressService: ShippingAddressService,
  ) {
    let strId = this.route.snapshot.paramMap.get('id');
    this.id = parseInt(strId ? strId : '');
    if (this.id) //daca are id-ul face getAddressId ca sa puna datele in camp
      this.shippingAddressService.getAddressId(this.id).pipe(take(1)).subscribe(a => this.shipping = a);
  }

  ngOnInit(): void {
  }

  save() {
    if (this.id)
      this.shippingAddressService.updateAddress(this.shipping);
    else
      this.shippingAddressService.addAddress(this.shipping);

    this.router.navigate(['/shippingAddressList']);


  }

  delete() {
    if (!confirm('Are you sure you want to delete this address?')) return;

    this.shippingAddressService.deleteAddress(this.id);

    this.router.navigate(['/shippingAddressList']);

  }

}
