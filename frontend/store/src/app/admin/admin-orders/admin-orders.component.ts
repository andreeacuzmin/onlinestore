import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/store/services/order.service';

@Component({
  selector: 'app-admin-orders',
  templateUrl: './admin-orders.component.html',
  styleUrls: ['./admin-orders.component.scss']
})
export class AdminOrdersComponent implements OnInit {

  orders$: any;

  constructor(private orderService: OrderService) {
    this.orders$ = orderService.getAllOrders();
  }

  confirmOrder(id: number) {
    this.orderService.confirmOrderById(id).subscribe(
      x => window.location.reload(),
      x => alert("Error confirming order")
    )
  }

  ngOnInit(): void {
  }

}
