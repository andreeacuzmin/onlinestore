import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { Product } from 'src/app/store/models/Product';
import { ProductService } from 'src/app/store/services/product.service';


@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.scss']
})
export class AdminProductsComponent implements OnInit, OnDestroy {
  products: Product[] = [];
  filteredProducts: Product[] = [];
  subscription: Subscription;


  constructor(private productService: ProductService) {
    this.subscription = this.productService.getProducts()
      .subscribe(products => {
        this.filteredProducts = this.products = products;

      });
  }

  filter(query: string) {
    // console.log(query);
    this.filteredProducts = (query) ?
      this.products.filter(p => p.productName.toLowerCase().includes(query.toLowerCase())) :
      this.products;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngOnInit() {
  }

}
