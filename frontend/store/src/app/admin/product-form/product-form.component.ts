import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/store/models/Product';
import { ProductService } from 'src/app/store/services/product.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {
  product: Product = new Product();
  id: number = 0;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductService) {

    let strId = this.route.snapshot.paramMap.get('id');
    this.id = parseInt(strId ? strId : '');
    if (this.id) //daca are id-ul face getProductById ca sa puna datele in camp
      this.productService.getProductById(this.id).pipe(take(1)).subscribe(p => this.product = p);
  }

  save() {
    if (this.id)
      this.productService.updateProduct(this.product);
    else
      this.productService.addProduct(this.product);

    this.router.navigate(['/admin/products']);
  }

  delete() {
    if (!confirm('Are you sure you want to delete this product?')) return;

    this.productService.deleteProduct(this.id);

    this.router.navigate(['/admin/products']);

  }

  ngOnInit(): void {
  }

}
