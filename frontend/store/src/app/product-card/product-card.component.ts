import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { CartItem, ProductInfo } from '../store/models/CartItem';
import { Product } from '../store/models/Product';
import { CartService } from '../store/services/cart.service';
import { TokenStorageService } from '../store/services/token-storage.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent {
  @Input('product')
  product!: Product;
  @Input('show-actions') showActions = true;
  // @Input('shopping-cart') shoppingCart;


  constructor(private router: Router, private cartService: CartService, private tokenStorageService: TokenStorageService) { }

  addToCart(product: Product) {
    let userId = this.tokenStorageService.getUser().id;
    if (!userId) {
      this.router.navigate(['/login']);
      return;
    }

    this.cartService.addCartItem(userId, new CartItem(1, new ProductInfo(product.productId)));
    window.location.reload();
  }

  // getQuantity(){

  //   let item = this.shoppingCart.cartItems[this.product.productId];
  //    return item ? item.quantity : 0;
  // }

}
