import { Component, OnInit } from '@angular/core';
import { RegistrationService } from '../store/services/registration.service';
import { TokenStorageService } from '../store/services/token-storage.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: any = {
    username: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';



  constructor(private authService: RegistrationService, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    if (this.tokenStorageService.getToken()) {
      this.isLoggedIn = true;
    }
  }


  onSubmit(): void {
    const { username, password } = this.form;

    this.authService.login(username, password).subscribe(
      data => {
        let token = data.headers.get('Authorization');
        console.log(token);

        this.tokenStorageService.saveToken(token);
        this.tokenStorageService.saveUser(data.body);

        this.isLoginFailed = false;
        this.isLoggedIn = true;

        this.reloadPage();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }

  getFullName(): string {
    return this.tokenStorageService.getUser().fullName;
  }

  getRoles(): string {
    return this.tokenStorageService.getUser().role;
  }

  reloadPage(): void {
    window.location.reload();
  }


}
