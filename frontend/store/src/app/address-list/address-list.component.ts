import { Component, OnInit } from '@angular/core';
import { ShippingAddress } from '../store/models/ShippingAddress';
import { ShippingAddressService } from '../store/services/shipping-address.service';
import { TokenStorageService } from '../store/services/token-storage.service';

@Component({
  selector: 'app-address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.scss']
})
export class AddressListComponent implements OnInit {
  shipping$: any;

  constructor(private shippingService: ShippingAddressService, private tokenStorageService: TokenStorageService) {
    let userId = this.tokenStorageService.getUser().id;
    this.shipping$ = this.shippingService.getAddressByUserId(userId);
  }

  ngOnInit(): void {
  }

}
