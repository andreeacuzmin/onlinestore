package onlinestore.project.exceptions;

public class OutOfStockException extends RuntimeException {
    public OutOfStockException(Integer quantity, Integer stock, Exception ex) {
        super("Cantitatea ceruta " + quantity + " este mai mare decat " + stock + " bucati disponibile!", ex);
    }
}
