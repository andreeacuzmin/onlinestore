package onlinestore.project.exceptions;

import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.logging.Logger;

@Controller
public class ExceptionHandlingController {

   @ExceptionHandler({
           ProductNotFoundException.class,
           OutOfStockException.class
   })
  // @ExceptionHandler(Exception.class)
    public String productNotFound(HttpServletRequest req, Exception ex){
        System.out.println("Request: " + req.getRequestURL() + " raised " + ex);
        return ex.getMessage();
    }

}
