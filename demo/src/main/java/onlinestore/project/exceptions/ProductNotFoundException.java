package onlinestore.project.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Product not found")
public class ProductNotFoundException extends RuntimeException {
    public ProductNotFoundException(Long id, Exception ex) {
        super("Produsul cu id-ul " + id + " nu exista!", ex);
    }
}

