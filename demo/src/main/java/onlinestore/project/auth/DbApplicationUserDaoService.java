package onlinestore.project.auth;

import onlinestore.project.model.Customer;
import onlinestore.project.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Repository("real")
public class DbApplicationUserDaoService implements ApplicationUserDao {

    private final PasswordEncoder passwordEncoder;
    private final CustomerService customerService;

    @Autowired
    public DbApplicationUserDaoService(PasswordEncoder passwordEncoder, CustomerService customerService) {
        this.passwordEncoder = passwordEncoder;
        this.customerService = customerService;
    }


    @Override
    public Optional<ApplicationUser> selectApplicationUserByUsername(String username) {

        Customer customerByNickname = customerService.getCustomerByNickname(username);
        if (customerByNickname == null) {
            return Optional.empty();
        }
        return Optional.of(new ApplicationUser(
                customerByNickname.getCustomerId(),
                customerByNickname.getCustomerName(),
                customerByNickname.getNickname(),
                passwordEncoder.encode(customerByNickname.getPassword()),
                customerByNickname.getRole(),
                customerByNickname.getRole().getGrantedAuthorities(),
                true,
                true,
                true,
                true
        ));

    }

    private List<ApplicationUser> getApplicationUser() {

        List<Customer> allCustomers = customerService.getAllCustomers();

        List<ApplicationUser> applicationUsers = allCustomers.stream()
                .map(c -> new ApplicationUser(
                        c.getCustomerId(),
                        c.getCustomerName(),
                        c.getNickname(),
                        passwordEncoder.encode(c.getPassword()),
                        c.getRole(),
                        c.getRole().getGrantedAuthorities(),
                        true,
                        true,
                        true,
                        true
                ))
                .collect(Collectors.toList());

        return applicationUsers;
    }
}
