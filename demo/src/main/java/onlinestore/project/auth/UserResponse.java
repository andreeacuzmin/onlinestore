package onlinestore.project.auth;

import lombok.Data;
import onlinestore.project.security.ApplicationUserRole;

@Data
public class UserResponse {

    private final String fullName;
    private final Long   id;
    private final ApplicationUserRole role;

    public UserResponse(Long id, String fullName, ApplicationUserRole role) {
        this.fullName = fullName;
        this.id = id;
        this.role = role;
    }
}
