package onlinestore.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class OnlinestoreApplication {

	public static void main(String[] args) {
//		System.out.println(args);
	SpringApplication.run(OnlinestoreApplication.class, args);

	}

}
