package onlinestore.project.security;

import com.google.common.collect.Sets;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static onlinestore.project.security.ApplicationUserPermission.*;


public enum ApplicationUserRole {
    CUSTOMER(Sets.newHashSet(CUSTOMER_READ,CUSTOMER_WRITE,PRODUCT_READ,ORDER_READ,ORDER_WRITE,CART_SELFREAD,CART_SELFWRITE)),
    ADMIN(Sets.newHashSet(CUSTOMER_READ,PRODUCT_READ,PRODUCT_WRITE,ORDER_READ,ORDER_WRITE,CART_ALLREAD,CART_ALLWRITE));

    private final Set<ApplicationUserPermission> permissions;

    ApplicationUserRole(Set<ApplicationUserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<ApplicationUserPermission> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getGrantedAuthorities(){
        Set<SimpleGrantedAuthority> permissions = getPermissions()
                .stream().map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
        permissions.add(new SimpleGrantedAuthority("ROLE_"+this.name()));
        return permissions;
    }

}
