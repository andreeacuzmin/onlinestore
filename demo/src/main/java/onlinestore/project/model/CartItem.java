package onlinestore.project.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Data
public class CartItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "cartItemId", updatable = false, nullable = false)
    private Long cartItemId;

    @ManyToOne
    private Cart cart;

    @ManyToOne
    private ProductInfo productInfo;


    private Integer quantity;

    @PreRemove
    public void removeCart(){

        this.cart=null;
    }

    @Transient
    public Double getTotal() {
        return getProductInfo().getProductPrice() * getQuantity();
    }

    public void setCartId(Long cartId) {
        setCart(new Cart(cartId));
    }

    public void addQuantity(Integer quantity){
        this.quantity+=quantity;

    }

}
