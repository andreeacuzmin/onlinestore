package onlinestore.project.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "cartId",
        resolver = EntityIdResolver.class,
        scope=Cart.class)
//@EqualsAndHashCode(exclude="customer")
public class Cart {

    @Id
    private Long cartId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customerId")
    @MapsId
    private Customer customer;

    @OneToMany(mappedBy = "cart", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<CartItem> cartItems = new ArrayList<>();


    public void removeCartItem(CartItem cartItem){
        cartItem.removeCart();
        this.cartItems.remove(cartItem);
    }
    @PreRemove
    public void removeAllCartItems(){
        this.cartItems.forEach(cartItem -> cartItem.removeCart());
        this.cartItems.clear();
    }

    @Transient
    public double getTotal() {
        double total = 0;
        for (CartItem ci : cartItems) {
            total += ci.getTotal();
        }
        return total;
    }

    public Cart() {}

    public Cart(Long cartId) {
        this.cartId = cartId;
    }

    public Cart(Customer customer) {
        setCartId(customer.getCustomerId());
        setCustomer(customer);
        customer.setCart(this);
    }

    @Override
    public String toString() {
        return "Cart{" +
                "cartId=" + cartId +
                ", total=" + getTotal() +
                '}';
    }
}
