package onlinestore.project.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import onlinestore.project.security.ApplicationUserRole;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "customers")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "customerId", resolver = EntityIdResolver.class,
        scope=Customer.class)
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "customerId", updatable = false, nullable = false)
    private Long customerId;

    @NotNull
    private String customerName;

    @NotNull
    @Column(unique = true)
    private String nickname;

    @Email(message = "Email must be valid")
    @Column(unique = true)
    @NotNull
    private String customerEmail;

    @Length(min=8,message = "Parola trebuie sa aiba min 8 caractere")
    private String password;

    @NotNull
    private ApplicationUserRole role = ApplicationUserRole.CUSTOMER;

    @Pattern(regexp = "^\\d{10}$", flags = Pattern.Flag.UNICODE_CASE,message = "Numarul de telefon trebuie sa aiba 10 cifre")
    @NotNull
    private String customerPhone;

    @OneToOne(mappedBy = "customer",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private Cart cart;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ShippingAddress> shippingAddressList = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Order> orders = new ArrayList<>();


}
