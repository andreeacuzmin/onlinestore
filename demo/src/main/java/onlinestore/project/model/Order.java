package onlinestore.project.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import onlinestore.project.enums.OrderStatus;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name="customer_order")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "customerOrderId", resolver = EntityIdResolver.class,
        scope=Order.class)
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "customerOrderId", updatable = false, nullable = false)
    private Long customerOrderId;

    @Enumerated(EnumType.ORDINAL)
    private OrderStatus orderStatus=OrderStatus.NEW;

    @ManyToOne
    private Customer customer;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<OrderItem> orderItems = new ArrayList<>();

    @ManyToOne
    private ShippingAddress shippingAddress;

    private Date datePlaced= new Date();


}
