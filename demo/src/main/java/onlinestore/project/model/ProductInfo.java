package onlinestore.project.model;

import com.sun.istack.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;


@Data
@Entity
@Table(name="product")

public class ProductInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "productId", updatable = false, nullable = false)
    private Long productId;

    @NotNull
    private String productName;

    private String productDescription;

    @NotNull
    private double productPrice;

    @NotNull
    private int productStock;

    private String imageUrl;

}
