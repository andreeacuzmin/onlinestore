package onlinestore.project.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.sun.istack.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "shippingAddressId", resolver = EntityIdResolver.class,
//        scope=ShippingAddress.class)
public class ShippingAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "shippingAddressId", updatable = false, nullable = false)
    private Long shippingAddressId;

    private String fullAddress;

    @ManyToOne
    @NotNull
    private Customer customer ;


}
