package onlinestore.project.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "orderItemId", updatable = false, nullable = false)
    private Long orderItemId;

    @ManyToOne
    private Order order;

    @ManyToOne
    private ProductInfo productInfo;

    private Integer quantity;

    private Double price;

}
