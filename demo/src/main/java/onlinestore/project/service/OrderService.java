package onlinestore.project.service;

import onlinestore.project.enums.OrderStatus;
import onlinestore.project.model.*;
import onlinestore.project.repository.OrderItemRepository;
import onlinestore.project.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private CartService cartService;

    @Autowired
    private ShippingAddressService shippingAddressService;

    public List<Order> getOrdersList() {
        return orderRepository.findAll();
    }

    public Order getOrderById(@PathVariable Long customerOrderId) {
        return orderRepository.findById(customerOrderId).get();
    }

    public void addOrder(Long customerId, Long shippingAddressId) {

        //primeste id customerului
        Cart cart = cartService.getByCartId(customerId);

        //primeste shipping address
        ShippingAddress shippingAddress = shippingAddressService.getAddressById(shippingAddressId);

        //fac un new Order
        Order newOrder = new Order();
        newOrder.setCustomer(cart.getCustomer());
        newOrder.setShippingAddress(shippingAddress);

        //save newOrder
        orderRepository.saveAndFlush(newOrder);

        // for each din cart ()
        for (CartItem ci : cart.getCartItems()) {
           OrderItem orderItem = new OrderItem();
           orderItem.setProductInfo(ci.getProductInfo());
           orderItem.setQuantity(ci.getQuantity());
           orderItem.setPrice(ci.getProductInfo().getProductPrice());
           orderItem.setOrder(newOrder);
           orderItemRepository.saveAndFlush(orderItem);
           //newOrder.getOrderItems().add(orderItem);
        }

        //clear cart
        cartService.removeAllCartItems(customerId);
    }

    public List<Order> findOrdersByCustomer(Long customerId) {
        return orderRepository.findOrdersByCustomer_CustomerId(customerId);
    }


    public void confirmOrderById(Long customerOrderId) {
        Order toConfirm = orderRepository.findById(customerOrderId).get();
        toConfirm.setOrderStatus(OrderStatus.FINISHED);
        orderRepository.saveAndFlush(toConfirm);
    }
}
