package onlinestore.project.service;

import onlinestore.project.model.Customer;
import onlinestore.project.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CartService cartService;

    public Customer addCustomer(Customer customer) {
        Customer newCustomer = customerRepository.saveAndFlush(customer);
        cartService.addCart(newCustomer);
        return newCustomer;
    }

    public Customer getCustomerById(Long customerId) {
        return customerRepository.getByCustomerId(customerId);
    }

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public Customer getCustomerByNickname(String nickname) {
        return customerRepository.getByNickname(nickname).get(0);
    }

    public void deleteCustomer(@PathVariable Long customerId) {
        customerRepository.deleteById(customerId);
    }
}
