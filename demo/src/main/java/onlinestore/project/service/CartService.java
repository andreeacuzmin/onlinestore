package onlinestore.project.service;

import onlinestore.project.exceptions.OutOfStockException;
import onlinestore.project.model.Cart;
import onlinestore.project.model.CartItem;
import onlinestore.project.model.Customer;
import onlinestore.project.model.ProductInfo;
import onlinestore.project.repository.CartItemRepository;
import onlinestore.project.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class CartService {

    @Autowired
    private CartRepository cartRepository;

    public Cart getByCartId(Long cartId) {
        return cartRepository.findById(cartId).get();
    }

    public Cart addCart(Customer customer) {
        Cart cart = new Cart(customer);
        return cartRepository.saveAndFlush(cart);
    }

    @Autowired
    private CartItemRepository cartItemRepository;

    @Autowired
    private ProductService productService;

    public Cart addCartItem(Long cartId, CartItem cartItem) throws Exception {
        ProductInfo product = productService.getProductById(cartItem.getProductInfo().getProductId());

        if (cartItem.getQuantity() > product.getProductStock()) {
            throw new OutOfStockException(cartItem.getQuantity(), product.getProductStock(), null);
        }

        //cautare dupa customerId si item si daca exista deaja sa se adauge cantitatea
        CartItem existingCartItem = cartItemRepository.getCartItemByCart_CartIdAndProductInfo_ProductId(cartId, product.getProductId());
        if (existingCartItem == null) {
            cartItem.setCartId(cartId);
            cartItemRepository.saveAndFlush(cartItem);
        } else {
//            existingCartItem.setQuantity(existingCartItem.getQuantity() + cartItem.getQuantity());
            existingCartItem.addQuantity(cartItem.getQuantity());
            cartItemRepository.saveAndFlush(existingCartItem);
        }

        product.setProductStock(product.getProductStock() - cartItem.getQuantity());
        productService.updateProduct(product);
        return getByCartId(cartId);
    }

    public void removeCartItem(Long id) {
        CartItem cartItem = cartItemRepository.getOne(id);
        ProductInfo product = cartItem.getProductInfo();
        product.setProductStock(product.getProductStock() + cartItem.getQuantity());
        productService.updateProduct(product);
        cartItemRepository.deleteById(id);

    }

    @Transactional
    public void removeAllCartItems(Long cartId) {
        Cart cart=cartRepository.getByCartId(cartId);
        for (CartItem cartItem : cart.getCartItems()) {
            ProductInfo product = cartItem.getProductInfo();
            product.setProductStock(product.getProductStock() + cartItem.getQuantity());
            productService.updateProduct(product);
        }

        cartItemRepository.deleteCartItemsByCart_CartId(cartId);
    }


}
