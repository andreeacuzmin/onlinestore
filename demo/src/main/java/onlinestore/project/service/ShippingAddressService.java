package onlinestore.project.service;

import onlinestore.project.model.ProductInfo;
import onlinestore.project.model.ShippingAddress;
import onlinestore.project.repository.ShippingAddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class ShippingAddressService {

    @Autowired
    ShippingAddressRepository shippingAddressRepository;

    public List<ShippingAddress> getAddressList() {
        return shippingAddressRepository.findAll();
    }

    public ShippingAddress addAddress(ShippingAddress shippingAddress){
        return shippingAddressRepository.saveAndFlush(shippingAddress);
    }

    public void deleteAddress(Long shippingAddressId) {
        shippingAddressRepository.deleteById(shippingAddressId);
    }

    public void updateAddress( ShippingAddress shippingAddress ){
        shippingAddressRepository.saveAndFlush(shippingAddress);
    }

    public List< ShippingAddress> getAddressesByUserId(Long customerId) {
       return shippingAddressRepository.findShippingAddressesByCustomer_CustomerId(customerId);
    }

    public ShippingAddress getAddressById(@PathVariable Long shippingAddressId){
        return shippingAddressRepository.findById(shippingAddressId).get();
    }
}
