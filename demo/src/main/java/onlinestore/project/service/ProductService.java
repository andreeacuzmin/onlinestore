package onlinestore.project.service;

import onlinestore.project.model.ProductInfo;
import onlinestore.project.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService{

    @Autowired
    private ProductRepository productRepository;

    public List<ProductInfo> getProductList() {
        return productRepository.findAll();
    }

    public ProductInfo addProduct(ProductInfo productInfo) {
        return productRepository.saveAndFlush(productInfo);
    }


    public ProductInfo getProductById(Long productId) {
        return productRepository.findById(productId).get();
    }

    public void deleteProduct(Long productId) {
        productRepository.deleteById(productId);
    }

    public void updateProduct( ProductInfo productInfo ){
        productRepository.saveAndFlush(productInfo);
    }
}
