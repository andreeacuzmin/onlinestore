package onlinestore.project.controller;

import onlinestore.project.exceptions.ExceptionHandlingController;
import onlinestore.project.model.Order;
import onlinestore.project.model.ShippingAddress;
import onlinestore.project.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController extends ExceptionHandlingController {

    @Autowired
    private OrderService orderService;

    @GetMapping
    public List<Order> getOrdersList() {
        return orderService.getOrdersList();
    }

    @GetMapping("/get/{customerOrderId}")
    public Order getOrderById(@PathVariable Long customerOrderId){
        return orderService.getOrderById(customerOrderId);
    }

    @PutMapping("/confirm/{customerOrderId}")
    public void confirmOrderById(@PathVariable Long customerOrderId){
        orderService.confirmOrderById(customerOrderId);
    }

    @PostMapping("/add/{customerId}/{shippingAddressId}")
    public void addOrder(@PathVariable Long customerId, @PathVariable Long shippingAddressId) {
        orderService.addOrder(customerId,shippingAddressId);
    }

    @GetMapping("/getByCustomer/{customerId}")
    public List<Order> findOrdersByCustomer(@PathVariable Long customerId){
       return orderService.findOrdersByCustomer(customerId);
    }

}
