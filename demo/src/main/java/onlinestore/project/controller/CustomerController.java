package onlinestore.project.controller;

import onlinestore.project.auth.DbApplicationUserDaoService;
import onlinestore.project.exceptions.ExceptionHandlingController;
import onlinestore.project.model.Customer;
import onlinestore.project.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/customers")
public class CustomerController extends ExceptionHandlingController {

    @Autowired
    CustomerService customerService;

    //hasRole('ROLE_') hasAnyRole('ROLE_') hasAuthority('permission') hasAnyAuthority('permission')

    @GetMapping
    //@PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Customer> getAllCustomers() {
        return customerService.getAllCustomers();
    }

    @PostMapping("/add")
    //@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_CUSTOMER')")
    public void addCustomer(@Valid @RequestBody Customer customer) {
        customerService.addCustomer(customer);
    }

    @GetMapping("/get/{id}")
   // @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_CUSTOMER')")

    public Customer getCustomerById(@PathVariable Long id) {
        Customer ci = customerService.getCustomerById(id);
        return ci;
    }

    @GetMapping("/getByName/{nickname}")
   // @PreAuthorize("hasRole('ROLE_ADMIN') or #nickname == authentication.name")
    public Customer getCustomerByNickname(@PathVariable String nickname) {
        return customerService.getCustomerByNickname(nickname);
    }

    @DeleteMapping
   // @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping("/delete/{id}")
    public void deleteCustomer(@PathVariable Long id) {
        customerService.deleteCustomer(id);
    }



}
