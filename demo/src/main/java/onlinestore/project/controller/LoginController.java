package onlinestore.project.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import onlinestore.project.model.Customer;
import onlinestore.project.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class LoginController {



//    @GetMapping("login")
//    public String getLoginView(){
//        return "login";
//    }


//    @PostMapping("login")
//    public Customer getCurrentUser(){
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        if ((authentication instanceof AnonymousAuthenticationToken)) {
//            String currentUserName = authentication.getName();
//            System.out.println(currentUserName);
//        }
//        return customerService.getCustomerById(4L);
//    }

    @Bean
    public static AuthenticationSuccessHandler authenticationSuccessHandler() {
        return (request, response, authentication) -> {
//            response.getWriter().write(new ObjectMapper().writeValueAsString(new UserAuthenticationResponse(authentication.getName(), 123l)));
            response.getWriter().write("bla bla bla");
            response.setStatus(200);
        };
    }

    //The simplest way to retrieve the currently authenticated principal is via a static call to the SecurityContextHolder
    //An improvement to this snippet is first checking if there is an authenticated user before trying to access it

}
