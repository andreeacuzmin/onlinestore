package onlinestore.project.controller;

import onlinestore.project.exceptions.ExceptionHandlingController;
import onlinestore.project.model.Cart;
import onlinestore.project.model.CartItem;
import onlinestore.project.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customer/cart")
public class CartController extends ExceptionHandlingController {

    @Autowired
    private CartService cartService;

    @GetMapping("/get/{id}")
    public Cart getCart(@PathVariable Long id) {
        return cartService.getByCartId(id);
    }

    @PostMapping("/add/{cartId}")
    public Cart addCartItem(@PathVariable Long cartId,
                            @RequestBody CartItem cartItem) throws Exception {
       return cartService.addCartItem(cartId,cartItem);
    }

    @DeleteMapping
    @RequestMapping("/delete/{id}")
    public void removeCartItem(@PathVariable Long id) {
        cartService.removeCartItem(id);
    }

    @DeleteMapping
    @RequestMapping("/clearCart/{cartId}")
    public void removeAllCartItems(@PathVariable Long cartId) {
        cartService.removeAllCartItems(cartId);
    }

}