package onlinestore.project.controller;

import onlinestore.project.exceptions.ExceptionHandlingController;
import onlinestore.project.exceptions.ProductNotFoundException;
import onlinestore.project.model.ProductInfo;
import onlinestore.project.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/product")
public class ProductController extends ExceptionHandlingController {
    @Autowired  private ProductService productService;

    @GetMapping
    public List<ProductInfo> getAllProducts() {
        return productService.getProductList();
    }

    @RequestMapping("/add")
    public ProductInfo addProduct(@RequestBody ProductInfo productInfo) {
        return productService.addProduct(productInfo);
    }

    @GetMapping("/get/{id}")
    public ProductInfo getProductById(@PathVariable Long id) {
        try {
            return productService.getProductById(id);
        }catch (NoSuchElementException exception){
            throw new ProductNotFoundException(id,exception);
        } }

    @DeleteMapping
    @RequestMapping("/delete/{id}")
    public void deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);
    }
    @PutMapping
    @RequestMapping("/update")
    public void updateProduct( @RequestBody ProductInfo productInfo){
        productService.updateProduct(productInfo);
    }

}
