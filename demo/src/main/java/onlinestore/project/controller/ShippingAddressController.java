package onlinestore.project.controller;

import onlinestore.project.model.ShippingAddress;
import onlinestore.project.service.ShippingAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shippingAddress")
public class ShippingAddressController {

    @Autowired
    private ShippingAddressService shippingAddressService;


    @GetMapping
    public List<ShippingAddress> getAddressList() {
        return shippingAddressService.getAddressList();
    }

    @GetMapping("/getByUserId/{customerId}")
    public List<ShippingAddress> getAddressesByUserId(@PathVariable Long customerId){
        return shippingAddressService.getAddressesByUserId(customerId);
    }

    @GetMapping("/get/{shippingAddressId}")
    public ShippingAddress getAddressById(@PathVariable Long shippingAddressId){
        return shippingAddressService.getAddressById(shippingAddressId);
    }

    @RequestMapping("/add")
    public ShippingAddress addAddress(@RequestBody ShippingAddress shippingAddress) {
        return shippingAddressService.addAddress(shippingAddress);
    }

    @DeleteMapping
    @RequestMapping("/delete/{shippingAddressId}")
    public void deleteAddress(@PathVariable Long shippingAddressId) {
        shippingAddressService.deleteAddress(shippingAddressId);
    }

    @PutMapping
    @RequestMapping("/update")
    public void updateAddress( @RequestBody ShippingAddress shippingAddress){
        shippingAddressService.updateAddress(shippingAddress);
    }
}
