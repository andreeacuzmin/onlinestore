package onlinestore.project.repository;

import onlinestore.project.model.ShippingAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShippingAddressRepository extends JpaRepository<ShippingAddress,Long> {

    List<ShippingAddress> findShippingAddressesByCustomer_CustomerId(Long customerId);
}
