package onlinestore.project.repository;

import onlinestore.project.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long> {
    Customer getByCustomerId(Long customerId);

    List<Customer> getByNickname(String nickname);



}
