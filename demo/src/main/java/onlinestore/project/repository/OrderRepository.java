package onlinestore.project.repository;

import onlinestore.project.model.Customer;
import onlinestore.project.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {
    List<Order> findOrdersByCustomer_CustomerId(Long customerId);
}
