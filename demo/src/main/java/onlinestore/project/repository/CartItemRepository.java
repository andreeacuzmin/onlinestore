package onlinestore.project.repository;

import onlinestore.project.model.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface CartItemRepository extends JpaRepository<CartItem, Long> {
    List<CartItem> getCartItemsByCart(Long cartId);

    void deleteCartItemsByCart_CartId(Long cartId);

    CartItem getCartItemByCart_CartIdAndProductInfo_ProductId(Long cartId, Long productId);

}
