package onlinestore.project.repository;

import onlinestore.project.model.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository
        extends JpaRepository<ProductInfo,Long> {

}
