package onlinestore.project.enums;

public interface CodeEnum {
    Integer getCode();
}
