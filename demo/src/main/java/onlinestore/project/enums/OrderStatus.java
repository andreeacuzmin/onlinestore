package onlinestore.project.enums;

public enum OrderStatus implements CodeEnum{

    NEW( "New Order"),
    PROCESSING( "Processing"),
    FINISHED( "Finished"),
    CANCELED("Canceled")
    ;

    private String msg;

    OrderStatus( String msg) {
        this.msg = msg;
    }


    @Override
    public Integer getCode() {
        return this.ordinal();
    }
}
